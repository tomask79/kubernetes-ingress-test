# Kubernetes and Ingress routing #

Finally after my Apache Spark and Apache Hive certification madness I found a time to get my hands on the Kubernetes ingress routing
on the services. I prepared a demo with **two Spring MVC projects** running on ports 8081 and 8082 with URL
paths **/sayhello and /saygoodbye**...Okay, here we go:

## Prerequisites ##

* Kubernetes cluster via [Minikube](https://kubernetes.io/docs/setup/minikube/)
* Docker
* [Ingress controller via nginx](https://github.com/kubernetes/ingress-nginx)

If you use minikube version > 0.14 then it has nginx as ingress controller    
already available. To enable it, simply run:    

    minikube addons enable ingress

After that verify that nginx controller has started as a pod:

    tomask79:ingress tomask79$ kubectl --namespace=kube-system get pods
    NAME                                        READY     STATUS    RESTARTS   AGE
    default-http-backend-59868b7dd6-rmdlg       1/1       Running   1          1d
    etcd-minikube                               1/1       Running   0          2h
    kube-addon-manager-minikube                 1/1       Running   8          97d
    kube-apiserver-minikube                     1/1       Running   0          2h
    kube-controller-manager-minikube            1/1       Running   0          2h
    kube-dns-86f4d74b45-xtvcw                   3/3       Running   27         97d
    kube-proxy-flh8s                            1/1       Running   0          2h
    kube-scheduler-minikube                     1/1       Running   6          92d
    kubernetes-dashboard-5498ccf677-hhr5r       1/1       Running   16         97d
    nginx-ingress-controller-67956bf89d-xckz4   1/1       Running   2          1d
    storage-provisioner                         1/1       Running   17         97d
    tomask79:ingress tomask79$ 

## Spring MVC controller and Kubernetes Ingress deploy ##

First MVC controller:    

    @RestController
    public class ControllerMVC {

        @RequestMapping("/saygoodbye")
        public String mvcTest() {
            return "I'm saying goodbye to Kubernetes!";
        }
    }

application.properties

    server.port=8082

Second MVC controller:    

    @RestController
    public class ControllerMVC {

        @RequestMapping("/sayhello")
        public String mvcTest() {
            return "I'm saying hello to Kubernetes!";
        }
    }

application.properties

    server.port=8081

Both projects have integrated [Maven Docker plugin](https://github.com/spotify/docker-maven-plugin) to create an Docker image.    
First let's do the packaging and building of the docker images.    

As usual, make sure you're using minikubes docker daemon first.

    tomask79:ingress tomask79$ eval $(minikube docker-env)

Then to start building the docker image run in both projects:

    tomask79:spring-boot-kubernetes-deploy tomask79$ mvn clean install

    tomask79:spring-boot-kubernetes-deploy2 tomask79$ mvn clean install

If everything went well then verify that both images    
**test-controller1 and test-controller2** are builded correctly:

    tomask79:ingress tomask79$ docker images
    REPOSITORY                                                       TAG                 IMAGE ID            CREATED             SIZE
    test-controller2                                                 1.0-SNAPSHOT        900c898a3e00        9 minutes ago       640MB
    test-controller1                                                 1.0-SNAPSHOT        19ac741628fa        11 minutes ago      640MB
    quay.io/kubernetes-ingress-controller/nginx-ingress-controller   0.19.0              22ebbdddfabb        3 weeks ago         414MB
    openjdk                                                          latest              a2fbe0dde8c0        3 months ago        624MB
    quay.io/kubernetes-ingress-controller/nginx-ingress-controller   0.14.0              036ea1188e19        4 months ago        287MB
    k8s.gcr.io/kube-proxy-amd64                                      v1.10.0             bfc21aadc7d3        6 months ago        97MB
    k8s.gcr.io/kube-controller-manager-amd64                         v1.10.0             ad86dbed1555        6 months ago        148MB
    k8s.gcr.io/kube-scheduler-amd64                                  v1.10.0             704ba848e69a        6 months ago        50.4MB
    k8s.gcr.io/kube-apiserver-amd64                                  v1.10.0             af20925d51a3        6 months ago        225MB
    k8s.gcr.io/etcd-amd64                                            3.1.12              52920ad46f5b        6 months ago        193MB
    k8s.gcr.io/kube-addon-manager                                    v8.6                9c16409588eb        7 months ago        78.4MB
    k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64                           1.14.8              c2ce1ffb51ed        8 months ago        41MB
    k8s.gcr.io/k8s-dns-sidecar-amd64                                 1.14.8              6f7f2dc7fab5        8 months ago        42.2MB
    k8s.gcr.io/k8s-dns-kube-dns-amd64                                1.14.8              80cc5ea4b547        8 months ago        50.5MB
    k8s.gcr.io/pause-amd64                                           3.1                 da86e6ba6ca1        9 months ago        742kB
    k8s.gcr.io/kubernetes-dashboard-amd64                            v1.8.1              e94d2f21bc0c        9 months ago        121MB
    gcr.io/k8s-minikube/storage-provisioner                          v1.8.1              4689081edb10        10 months ago       80.8MB
    gcr.io/google_containers/defaultbackend                          1.4                 846921f0fe0e        11 months ago       4.84MB
    k8s.gcr.io/defaultbackend                                        1.4                 846921f0fe0e        11 months ago       4.84MB
    k8s.gcr.io/echoserver                                            1.4                 a90209bb39e3        2 years ago         140MB
    tomask79:ingress tomask79$ 

If docker images are ready in the minikube's docker daemon    
then we're going to deploy both controllers into Kubernetes,    
first deployment resources then clusterIP services:

    tomask79:spring-boot-kubernetes-deploy tomask79$ kubectl create -f deployment.yaml 
    deployment.extensions "sayhello-container" created
    tomask79:spring-boot-kubernetes-deploy tomask79$ 

    tomask79:spring-boot-kubernetes-deploy tomask79$ kubectl create -f service.yaml 
    service "sayhello-service" created
    tomask79:spring-boot-kubernetes-deploy tomask79$ 
    
Deploying of the second Spring MVC controller:

    tomask79:spring-boot-kubernetes-deploy2 tomask79$ kubectl create -f deployment.yaml 
    deployment.extensions "saygoodbye-container" created
    tomask79:spring-boot-kubernetes-deploy2 tomask79$ 

    tomask79:spring-boot-kubernetes-deploy2 tomask79$ kubectl create -f service.yaml 
    service "saygoodbye-service" created
    tomask79:spring-boot-kubernetes-deploy2 tomask79$ 

Verification of the deployment, first deployment resources, notice the sayhello and goodbye containers:

    tomask79:spring-boot-kubernetes-deploy2 tomask79$ kubectl get deployments
    NAME                   DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    hello-minikube         1         1         1            1           96d
    saygoodbye-container   1         1         1            1           2m
    sayhello-container     1         1         1            1           4m
    tomask79:spring-boot-kubernetes-deploy2 tomask79$ 

service exposure has to be through ClusterIP services:

    tomask79:spring-boot-kubernetes-deploy2 tomask79$ kubectl get services
    NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
    kubernetes           ClusterIP   10.96.0.1      <none>        443/TCP    96d
    saygoodbye-service   ClusterIP   10.103.54.49   <none>        8082/TCP   2m
    sayhello-service     ClusterIP   10.109.92.51   <none>        8081/TCP   4m
    tomask79:spring-boot-kubernetes-deploy2 tomask79$ 

Okay, we've got Spring MVC controllers running at ports 8081 and 8082.     
Now let's define ingress resource (ingress.yml) for that, huge benefit of the ingress
routing is that it can be defined separately to K8s services, not like Nodeport or Loadbalancer.

    tomask79:ingress tomask79$ cat ingress.yml 
    apiVersion: extensions/v1beta1
    kind: Ingress
    metadata:
      name: example-ingress
      annotations:
        ingress.kubernetes.io/rewrite-target: /
    spec:
      rules:
      - http:
          paths:
            - path: /sayhello
              backend:
                serviceName: sayhello-service
                servicePort: 8081
            - path: /saygoodbye
              backend:
                serviceName: saygoodbye-service
                servicePort: 8082

Deploy of the mentioned ingress.yml:

    tomask79:ingress tomask79$ kubectl create -f ingress.yml 
    ingress.extensions "example-ingress" created
    tomask79:ingress tomask79$ 

It's important to check and verify the details regarding the routing roules:

    tomask79:ingress tomask79$ kubectl describe ingress example-ingress
    Name:             example-ingress
    Namespace:        default
    Address:          10.0.2.15
    Default backend:  default-http-backend:80 (172.17.0.8:8080)
    Rules:
      Host  Path  Backends
      ----  ----  --------
      *     
            /sayhello     sayhello-service:8081 (<none>)
            /saygoodbye   saygoodbye-service:8082 (<none>)
    Annotations:
      ingress.kubernetes.io/rewrite-target:  /
    Events: 
      Type    Reason  Age   From                      Message
      ----    ------  ----  ----                      -------
      Normal  CREATE  1m    nginx-ingress-controller  Ingress default/example-ingress
      Normal  CREATE  1m    nginx-ingress-controller  Ingress default/example-ingress
      Normal  UPDATE  1m    nginx-ingress-controller  Ingress default/example-ingress
      Normal  UPDATE  1m    nginx-ingress-controller  Ingress default/example-ingress

Everything ready! Let's test it:

    tomask79:ingress tomask79$ minikube ip
    192.168.99.101
    tomask79:ingress tomask79$ curl -kL http://192.168.99.101/sayhello
    I'm saying hello to Kubernetes!
    tomask79:ingress tomask79$ 
    tomask79:ingress tomask79$ curl -kL http://192.168.99.101/saygoodbye
    I'm saying goodbye to Kubernetes!
    tomask79:ingress tomask79$ 

As you can see Nginx as front-end controller to Kubernetes services works perfectly.    
Yes, by default nginx redirects every HTTP request to HTTPS,    
hence -kL options to curl. To customize this behaviour check:    

[ingress-nginx annotations](https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md) configuration.

Definitely play with it!

Best regards

Tomas