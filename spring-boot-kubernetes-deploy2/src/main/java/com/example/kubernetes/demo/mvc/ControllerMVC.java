package com.example.kubernetes.demo.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tomask79 on 20.06.18.
 */
@RestController
public class ControllerMVC {

    @RequestMapping("/saygoodbye")
    public String mvcTest() {
        return "I'm saying goodbye to Kubernetes!";
    }
}
