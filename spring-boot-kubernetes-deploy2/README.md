**Deploying Spring-Boot application into KUBERNETES cluster**


**Prerequisities**: 

Minikube : https://github.com/kubernetes/minikube

kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/

docker: https://docs.docker.com/toolbox/toolbox_install_mac/


**1. Simple Spring MVC controller and Docker image build with SPOTIFY maven plugin**        

..

    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RestController;

    /**
     * Created by tomask79 on 20.06.18.
     */
    @RestController
    public class ControllerMVC {

        @RequestMapping("/sayhello")
        public String mvcTest() {
            return "I'm saying hello to Kubernetes!";
        }
    }

No rocket science here, on http://<hostIP>:8081/sayhello app will return string "I'm saying hello to Kubernetes!"

***Docker part***

- git clone https://tomask79@bitbucket.org/tomask79/spring-boot-kubernetes-deploy.git
- Before building the image run: ***"eval $(minikube docker-env)"*** to connect the docker daemon to the minikube
- Run ***"mvn clean install"***. This will build the app JAR and docker image test-controller:1.0-SNAPSHOT.

...

    [INFO] --- docker-maven-plugin:1.0.0:build (build-image) @ demo ---
    [INFO] Using authentication suppliers: [ConfigFileRegistryAuthSupplier]
    [INFO] Copying /Users/tomask79/Downloads/kubernetes-boot/target/demo-0.0.1-SNAPSHOT.jar -> /Users/tomask79/Downloads/kubernetes-boot/target/docker/demo-0.0.1-SNAPSHOT.jar
    [INFO] Building image test-controller:1.0-SNAPSHOT
    Step 1/3 : FROM openjdk:latest

     ---> a2fbe0dde8c0
    Step 2/3 : ADD /demo-0.0.1-SNAPSHOT.jar //

     ---> f6d0ea73d221
    Step 3/3 : ENTRYPOINT ["java", "-jar", "/demo-0.0.1-SNAPSHOT.jar"]

     ---> Running in ef86f44eb22c
    Removing intermediate container ef86f44eb22c
     ---> f9079973efda
    ProgressMessage{id=null, status=null, stream=null, error=null, progress=null, progressDetail=null}
    Successfully built f9079973efda
    Successfully tagged test-controller:1.0-SNAPSHOT

If you want to change the image name and other properties change maven docker plugin properties:

            <plugin>
				<executions>
					<execution>
						<id>build-image</id>
						<phase>package</phase>
						<goals>
							<goal>build</goal>
						</goals>
					</execution>
				</executions>
				<groupId>com.spotify</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>1.0.0</version>
				<configuration>
					<imageName>test-controller:1.0-SNAPSHOT</imageName>
					<baseImage>openjdk:latest</baseImage>
					<entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
					<resources>
						<resource>
							<targetPath>/</targetPath>
							<directory>${project.build.directory}</directory>
							<include>${project.build.finalName}.jar</include>
						</resource>
					</resources>
				</configuration>
			</plugin>

Now run ***docker images***. You should see images of the kubernetes cluster components including our
test-controller:1.0-SNAPSHOT    
...

    tomask79:kubernetes-boot tomask79$ docker images
    REPOSITORY                                 TAG                 IMAGE ID            CREATED             SIZE
    test-controller                            1.0-SNAPSHOT        f59ad695afd9        About an hour ago   640MB
    openjdk                                    latest              a2fbe0dde8c0        2 weeks ago         624MB
    k8s.gcr.io/kube-proxy-amd64                v1.10.0             bfc21aadc7d3        2 months ago        97MB
    k8s.gcr.io/kube-apiserver-amd64            v1.10.0             af20925d51a3        2 months ago        225MB
    k8s.gcr.io/kube-controller-manager-amd64   v1.10.0             ad86dbed1555        2 months ago        148MB
    k8s.gcr.io/kube-scheduler-amd64            v1.10.0             704ba848e69a        2 months ago        50.4MB
    k8s.gcr.io/etcd-amd64                      3.1.12              52920ad46f5b        3 months ago        193MB
    k8s.gcr.io/kube-addon-manager              v8.6                9c16409588eb        4 months ago        78.4MB
    k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64     1.14.8              c2ce1ffb51ed        5 months ago        41MB
    k8s.gcr.io/k8s-dns-sidecar-amd64           1.14.8              6f7f2dc7fab5        5 months ago        42.2MB
    k8s.gcr.io/k8s-dns-kube-dns-amd64          1.14.8              80cc5ea4b547        5 months ago        50.5MB
    k8s.gcr.io/pause-amd64                     3.1                 da86e6ba6ca1        6 months ago        742kB
    k8s.gcr.io/kubernetes-dashboard-amd64      v1.8.1              e94d2f21bc0c        6 months ago        121MB
    gcr.io/k8s-minikube/storage-provisioner    v1.8.1              4689081edb10        7 months ago        80.8MB
    k8s.gcr.io/echoserver                      1.4                 a90209bb39e3        2 years ago         140MB

***Kubernetes part***

- I suppose you started the minikube by ***"minikube start"***
- Verify that minikube local kubernetes cluster is up by ***kubectl cluster-info***

...

    tomask79:kubernetes-boot tomask79$ kubectl cluster-info
    Kubernetes master is running at https://192.168.99.100:8443
    KubeDNS is running at https://192.168.99.100:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

It's time do deploy the app to kubernetes. Before I start I recommend to be familiar with the terms. 
Start here for example: https://kubernetes.io/docs/concepts/

Focus on the terms:

- Kubernetes POD
- Kubernetes Deployment
- Kubernetes Service (Type NodePort)

In the root directory with pom.xml run ***"kubectl create -f deployment.yaml"*** 

***deployment.yaml***

...

    apiVersion: extensions/v1beta1
    kind: Deployment
    metadata:
      name: test-app
    spec:
     replicas: 1
      template: 
        metadata:
          labels:
            app: test-app
        spec:
          containers:
          - name: test-app
             image: test-controller:1.0-SNAPSHOT
            ports:
            - containerPort: 8081

and verify that deployment is installed and POD is running.

...

     tomask79:kubernetes-boot tomask79$ kubectl get pods
     NAME                             READY     STATUS    RESTARTS   AGE
     hello-minikube-6c47c66d8-gzvgc   1/1       Running   4          5d
     test-app-64f8b5bfcd-vndtn        1/1       Running   0          2h

Verify that DEPLOYMENT with replicas is also running:

...

    tomask79:kubernetes-boot tomask79$ kubectl get deployments
    NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    hello-minikube   1         1         1            1           5d
    test-app         1         1         1            1           2h

To access the DEPLOYMENT test-app externally, we need to install kubernetes service for that.
I'm going to use NODEPORT type with randomly assigned port from range 30000-32767.

***service.yaml***
...

    apiVersion: v1
    kind: Service
    metadata:
      name: test-app
      labels:
        name: test-app
      spec:
        type: NodePort
      ports:
          - port: 8081
            targetPort: 8081
      selector:
       app: test-app

To install the service run ***"kubectl create -f service.yaml"***. Now let's verify it:
...

    tomask79:kubernetes-boot tomask79$ kubectl get services
    NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
    kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP          5d
    test-app     NodePort    10.107.38.160   <none>        8081:31201/TCP   2h
    tomask79:kubernetes-boot tomask79$ 

***Accessing the Deployment Externally***

To get the URL for the service we've got two options:

1) minikube service test-app --url

    tomask79:kubernetes-boot tomask79$ minikube service test-app --url
    http://192.168.99.100:31201
...................................................

2) kubectl describe service test-app
...

    tomask79:kubernetes-boot tomask79$ kubectl describe service test-app
    Name:                     test-app
    Namespace:                default
    Labels:                   name=test-app
    Annotations:              none
    Selector:                 app=test-app
    Type:                     NodePort
    IP:                       10.107.38.160
    Port:                     unset  8081/TCP
    TargetPort:               8081/TCP
    NodePort:                 unset  31201/TCP
    Endpoints:                172.17.0.5:8081
    Session Affinity:         None
    External Traffic Policy:  Cluster
    Events:                   none

We know that master is running at ***192.168.99.100*** and assigned port is 31201.
So let's test it:    
...

    tomask79:kubernetes-boot tomask79$ curl http://192.168.99.100:31201/sayhello
    I'm saying hello to Kubernetes!


Okay, we have succesfully deployed spring-boot application into kubernetes...I hope you liked it.

Regards

Tomas